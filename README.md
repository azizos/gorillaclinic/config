# GorillaClinic: configs
This project is used as a _GitOps_ repository (managed by FluxCD and Helm-Operator running in Kubernetes) that watches changes Helm templates (`gorillaclinic` directory).